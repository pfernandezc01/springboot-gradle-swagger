package es.pedcod.main.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Unit Test Class for configuration class to test to test only you
 */
@DisplayName("Configuration to OpenAPI")
@TestMethodOrder(Alphanumeric.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter(AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
class SwaggerConfigTest {

	/**
	 * The principal component to test
	 */
	SwaggerConfig configuration;

	@Nested
	@DisplayName("Docket Bean")
	class testDocket {

		@Test
		@Disabled
		@DisplayName("Default instance must not be null")
		void testExistsElements() {
			final Docket actualBean = getConfiguration().getApiBean();

			assertThat(actualBean).isNotNull();
		}
	}
}
